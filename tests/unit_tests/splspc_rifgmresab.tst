//===========================================================================//
// Copyright (C) 2011 - DIGITEO - Michael Baudin                             //
// Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  //
//                                                                           //
// This file must be used under the terms of the CeCILL.                     //
// This source file is licensed as described in the file COPYING, which      //
// you should have received as part of this distribution.  The terms         //
// are also available at                                                     //
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //
//===========================================================================//

// test 1 : a small matrix (control) (overdetermined)
A=[ -1.    3.    0.    0.    4.    0.  
	 2.   -5.    0.    0.    0.    1.  
	 0.    0.   -2.    3.    0.    0.  
     0.    0.    7.   -1.    0.    0.  
    -3.    0.    0.    4.    6.    0.
	 0.    5.    0.    0.   -7.    8.
	-1     0     0     7     2     0
	 0    -4     3     0     5    -9 ];
A=sparse(A);
xexp = (1:size(A,"c"))';
b = A*xexp; 
trif=1D-3; 
maxiter=3000;
restart=1000;
tol=1.e-6;
//
x=splspc_rifgmresab(A,b);
assert_checkalmostequal(x,xexp,0,1D-4);
//
nbiterexp=2;
[x,nbiter,resid]=splspc_rifgmresab(A,b,trif,restart,maxiter,tol);
assert_checkequal(nbiter,nbiterexp);
assert_checktrue(resid < 1.e-6);
assert_checkalmostequal(x,xexp,0,1D-4);
//
[x2,nbiter2,resid2]=splspc_rifgmresab(A,b,[],[],[],[]);
assert_checkequal(nbiter,nbiter2);
assert_checkequal(resid,resid2);
assert_checkequal(x,x2);
//
[x3,nbiter3,resid3]=splspc_rifgmresab(A,b);
assert_checkequal(nbiter,nbiter3);
assert_checkequal(resid,resid3);
assert_checkequal(x,x3);

//test 2 : a large sparse matrix, illc1850
path = fullfile(splspc_getpath(),"tests","matrices");
filename = fullfile(path,"illc1850.mtx");
A = mmread(filename);
A = A';
n = size(A,2);
e = ones(n,1);
b = A * e;

[x,nbiter,resid]=splspc_rifgmresab(A,b);
assert_checktrue(nbiter < maxiter);
assert_checktrue(resid < tol);
assert_checkalmostequal(norm(b-A*x),norm(b-A*e),0,1.e-4);

[x2,nbiter2,resid2]=splspc_gmresab(A,b);
assert_checktrue(nbiter < nbiter2);
assert_checkalmostequal(norm(b-A*x2),norm(b-A*e),0,1.e-4);

//breakdown (in preconditioning process)
A=[1 2;1 2;1 2];
A=sparse(A);
A=A';
m=size(A,1);
b = ones(m,1);
name="splspc_rifgmresab";
msg1="%s: preconditioning process breaks down because of non-full rank matrix \n";
assert_checkerror("[x,nbiter,resid]=splspc_rifgmresab(A,b)", msg1, [], name);

