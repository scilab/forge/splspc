//===========================================================================//
// Copyright (C) 2011 - DIGITEO - Michael Baudin                             //
// Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  //
//                                                                           //
// This file must be used under the terms of the CeCILL.                     //
// This source file is licensed as described in the file COPYING, which      //
// you should have received as part of this distribution.  The terms         //
// are also available at                                                     //
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //
//===========================================================================//

// test 1 : a small matrix (control) (overdetermined)
A=[ -1.    3.    0.    0.    4.    0.  
	 2.   -5.    0.    0.    0.    1.  
	 0.    0.   -2.    3.    0.    0.  
     0.    0.    7.   -1.    0.    0.  
    -3.    0.    0.    4.    6.    0.
	 0.    5.    0.    0.   -7.    8.
	-1     0     0     7     2     0
	 0    -4     3     0     5    -9 ];
A=sparse(A);
xexp = (1:size(A,"c"))';
b = A*xexp; 
tgrev=1D-3;
tsgrev=1D-6; 
maxiter=3000;
restart=1000;
tol=1.e-6;
nbiterexp=3;
//
x=splspc_grevgmresba(A,b);
assert_checkalmostequal(x,xexp,0,1D-7);
//
[x,nbiter,resid]=splspc_grevgmresba(A,b,tgrev,tsgrev,restart,maxiter,tol);
assert_checkequal(nbiter,nbiterexp);
assert_checktrue(resid < 1.e-6);
assert_checkalmostequal(x,xexp,0,1D-7);
//
[x2,nbiter2,resid2]=splspc_grevgmresba(A,b,[],[],[],[],[]);
assert_checkequal(nbiter,nbiter2);
assert_checkequal(resid,resid2);
assert_checkequal(x,x2);
//
[x3,nbiter3,resid3]=splspc_grevgmresba(A,b);
assert_checkequal(nbiter,nbiter3);
assert_checkequal(resid,resid3);
assert_checkequal(x,x3);

//test 2 : a large sparse matrix, illc1850
path = fullfile(splspc_getpath(),"tests","matrices");
filename = fullfile(path,"illc1850.mtx");
A = mmread(filename);
m = size(A,1);
b = rand(m,1);

[x,nbiter,resid] = splspc_grevgmresba(A,b);
assert_checktrue(nbiter < maxiter);
assert_checktrue(resid < tol);

[x2,nbiter2,resid2] = splspc_gmresba(A,b);
assert_checktrue(nbiter < nbiter2);
assert_checkalmostequal(norm(b-A*x),norm(b-A*x2),1.e-2);

x3=A\b;
assert_checkalmostequal(norm(b-A*x),norm(b-A*x3),1.e-2);

//test 3 : a large sparse rank deficient matrix, Maragal_3
path = fullfile(splspc_getpath(),"tests","matrices");
filename = fullfile(path, "Maragal_3_.mtx");
A = mmread(filename);
m = size(A,1);
b = rand(m,1);
[x,nbiter,resid] = splspc_grevgmresba(A,b);
assert_checktrue(nbiter < maxiter);
assert_checktrue(resid < tol);

[x2,nbiter2,resid2] = splspc_gmresba(A,b);
assert_checktrue(nbiter < nbiter2);
assert_checkalmostequal(norm(b-A*x),norm(b-A*x2),1.e-2);
