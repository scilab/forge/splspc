Scilab Splspc Toolbox

Sparse Least Squares Pre Conditioned methods

Purpose
-------

The goal of this toolbox is to provide iterative methods for sparse linear least squares problems. This methods work with the iterative GMRES method for both underdetermined and overdetermined problems and use various preconditionning algorithms.

This module provides :
 * AB-GMRES and BA-GMRES without any particular preconditionner,
 * Robust Incomplete Factorization (RIF) preconditioning method working with AB-GMRES and BA-GMRES,
 * Greville's preconditioning method working with AB-GMRES and BA-GMRES,
 * inner iterations preconditioning process with AB-GMRES and BA-GMRES.

Features
--------

 * splspc_gmresab : the AB-GMRES iterative solver
 * splspc_gmresba : the BA-GMRES iterative solver
 * splspc_rifgmresab : the AB-GMRES iterative solver with a RIF preconditioner
 * splspc_rifgmresab : the BA-GMRES iterative solver with a RIF preconditioner
 * splspc_grevgmresab: the AB-GMRES iterative solver with a GREVILLE preconditioner
 * splspc_grevgmresba: the BA-GMRES iterative solver with a GREVILLE preconditioner
 * splspc_nrsorgmresba: the BA-GMRES iterative solver with an inner iteration preconditioner

Dependencies
------------

 * This module depends on the "MatrixMarket" module.
 * This module depends on the "assert" module.
 * This module depends on the "apifun" module.
 * This module depends on the "helptbx" module.

TODO
----

 * add INNER iterations with AB-GMRES and BA-GMRES
 * (eventually) add RIF, GREV and INNER functions without GMRES 
 * Test with A==[].

Licence
-------

This toolbox is released under the terms of the CeCILL license :
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Authors
-------

 * Copyright (C) 2007 - National Institute of Informatics - Ken Hayami, Tokushi Ito and Jun-Feng Yin
 * Copyright (C) 2009 - National Institute of Informatics - Ken Hayami, Xiaoke Cui and Jun-Feng Yin
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert

Bibliography
------------

 * "GMRES methods for least square problems", 
   K. Hayami, T. Ito and J.-F. Yin, 
   SIAM J. Matrix Anal. Appl., Vol. 31 (2010), No. 5, pp. 2400-2430
 * "Greville's method for preconditioning least square problems", 
   K. Hayami, T. Ito and J.-F. Yin, 2009, NII technical report
 * "Inner Iteration Krylov Subspace Methods for Least Squares Problems", 
   National Institute of Informatics, NII Technical Report, 
   Keichi Morikuni, Ken Hayami, NII-2011-001E, April 2011
 * Cui, X., Hayami K., and Yin, J.-F., 
   Greville�s method for preconditioning least squares problems, 
   Advances in Computational Mathematics , Vol. 35, pp. 243-269, 2011.
 * Morikuni, K. and Hayami, K., 
   Inner-iteration Krylov subspace methods for least squares problems, 
   NII Technical Reports, National Institute of Informatics, 
   Tokyo, NII-2011-001E, pp. 1-27, April, 2011. 
   http://www.nii.ac.jp/TechReports/11-001E.html



   