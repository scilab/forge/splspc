/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*             ********   ***                                 SparseLib++    */
/*          *******  **  ***       ***      ***                              */
/*           *****      ***     ******** ********                            */
/*            *****    ***     ******** ********              R. Pozo        */
/*       **  *******  ***   **   ***      ***                 K. Remington   */
/*        ********   ********                                 A. Lumsdaine   */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                                           */
/*                                                                           */
/*                     SparseLib++ : Sparse Matrix Library                   */
/*                                                                           */
/*               National Institute of Standards and Technology              */
/*                        University of Notre Dame                           */
/*              Authors: R. Pozo, K. Remington, A. Lumsdaine                 */
/*                                                                           */
/*                                 NOTICE                                    */
/*                                                                           */
/* Permission to use, copy, modify, and distribute this software and         */
/* its documentation for any purpose and without fee is hereby granted       */
/* provided that the above notice appear in all copies and supporting        */
/* documentation.                                                            */
/*                                                                           */
/* Neither the Institutions (National Institute of Standards and Technology, */
/* University of Notre Dame) nor the Authors make any representations about  */
/* the suitability of this software for any purpose.  This software is       */
/* provided ``as is'' without expressed or implied warranty.                 */
/*                                                                           */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


#ifndef _IOHB_H_
#define _IOHB_H_

#ifdef _MSC_VER
	#if LIBSPLSPC_EXPORTS 
		#define SPLSPC_IMPORTEXPORT __declspec (dllexport)
	#else
		#define SPLSPC_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define SPLSPC_IMPORTEXPORT
#endif

#ifdef __ProtoGlarp__
#undef __ProtoGlarp__
#endif
#if defined(__STDC__) || defined(__cplusplus)
#define __ProtoGlarp__(x) x
#else
#define __ProtoGlarp__(x) ()
#endif
 
#ifdef __cplusplus
extern "C" {
#endif

SPLSPC_IMPORTEXPORT void readHB_info        __ProtoGlarp__(( const char*, int*, int*, int*, int* ));
SPLSPC_IMPORTEXPORT void readHB_mat_double  __ProtoGlarp__(( const char*, int*, int*, double*));
SPLSPC_IMPORTEXPORT void readHB_mat_float   __ProtoGlarp__(( const char*, int*, int*, float*));

SPLSPC_IMPORTEXPORT void readHB_rhs_double __ProtoGlarp__(( const char*, double*, int));
SPLSPC_IMPORTEXPORT void readHB_rhs_float  __ProtoGlarp__(( const char*, float*, int));

SPLSPC_IMPORTEXPORT void writeHB_mat_double 
    __ProtoGlarp__(( const char*, int, int, int, const int*, const int*, 
            const double*, int, const double*, const char*, const char*));
SPLSPC_IMPORTEXPORT void writeHB_mat_float  
    __ProtoGlarp__(( const char*, int, int, int, const int*, const int*, 
            const float*, int, const double*, const char*, const char*));

#ifdef __cplusplus
}
#endif


#endif
