// ========================================================================
// Copyright (C) 2011 - DIGITEO   - Michael Baudin
// Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ========================================================================

src_dir   = get_absolute_file_path("builder_src.sce");
src_path  = "src";
linknames = ["sparselib"];
files = [
	  "compcol_double.cpp"
	  "comprow_double.cpp"
	  "coord_double.cpp"
	  "iotext_double.cpp"
	  "diagpre_double.cpp"
	  "icpre_double.cpp"
	  "ilupre_double.cpp"
	  "qsort_double.cpp"
	  "qsort_int.cpp"
	  "iohb_double.cpp"
	  "iohb.c"				
  ];

ldflags = "";

if MSDOS then
  include1 = src_dir+"..\include";
  include2 = src_dir+"..\..\mv\include";
  cflags = "-DWIN32 -DLIBSPLSPC_EXPORTS -I"""+...
		   include1+""" -I"""+include2+"""";
  libs     = [
			  src_dir+"..\..\spblas\libspblas"
			  src_dir+"..\..\mv\src\libmv"
			  ];	  
else
  include1 = src_dir;
  include2 = src_dir+"../include";
  include3 = src_dir+"../../mv/include";
  cflags = "-ansi -g -Wall -pedantic -O3 -I"""+...
		   include1+""" -I """+include2+""" -I """+include3+"""";
  libs = [
		  src_dir+"../../spblas/libspblas"
		  src_dir+"../../mv/src/libmv"
		  ];	  
end

tbx_build_src(linknames, files, src_path, src_dir, libs, ldflags, cflags);

clear src_dir src_path linknames files ldflags tbx_build_src;

