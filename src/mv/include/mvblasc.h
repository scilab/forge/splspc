
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                                           */
/*                                                                           */
/*                   MV++ Numerical Matrix/Vector C++ Library                */
/*                             MV++ Version 1.5                              */
/*                                                                           */
/*                                  R. Pozo                                  */
/*               National Institute of Standards and Technology              */
/*                                                                           */
/*                                  NOTICE                                   */
/*                                                                           */
/* Permission to use, copy, modify, and distribute this software and         */
/* its documentation for any purpose and without fee is hereby granted       */
/* provided that this permission notice appear in all copies and             */
/* supporting documentation.                                                 */
/*                                                                           */
/* Neither the Institution (National Institute of Standards and Technology)  */
/* nor the author makes any representations about the suitability of this    */
/* software for any purpose.  This software is provided ``as is''without     */
/* expressed or implied warranty.                                            */
/*                                                                           */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#ifndef _MV_BLAS1_COMPLEX_H_
#define _MV_BLAS1_COMPLEX_H_

#ifdef _MSC_VER
	#if LIBSPLSPC_EXPORTS 
		#define SPLSPC_IMPORTEXPORT __declspec (dllexport)
	#else
		#define SPLSPC_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define SPLSPC_IMPORTEXPORT
#endif

#define COMPLEX std::complex<double> 

SPLSPC_IMPORTEXPORT MV_Vector_COMPLEX& operator*=(MV_Vector_COMPLEX &x, const COMPLEX &a);
SPLSPC_IMPORTEXPORT MV_Vector_COMPLEX operator*(const COMPLEX &a, const MV_Vector_COMPLEX &x);
SPLSPC_IMPORTEXPORT MV_Vector_COMPLEX operator*(const MV_Vector_COMPLEX &x, const COMPLEX &a);
SPLSPC_IMPORTEXPORT MV_Vector_COMPLEX operator+(const MV_Vector_COMPLEX &x, 
    const MV_Vector_COMPLEX &y);
SPLSPC_IMPORTEXPORT MV_Vector_COMPLEX operator-(const MV_Vector_COMPLEX &x, 
    const MV_Vector_COMPLEX &y);
SPLSPC_IMPORTEXPORT MV_Vector_COMPLEX& operator+=(MV_Vector_COMPLEX &x, const MV_Vector_COMPLEX &y);
SPLSPC_IMPORTEXPORT MV_Vector_COMPLEX& operator-=(MV_Vector_COMPLEX &x, const MV_Vector_COMPLEX &y);

SPLSPC_IMPORTEXPORT COMPLEX dot(const MV_Vector_COMPLEX &x, const MV_Vector_COMPLEX &y);
SPLSPC_IMPORTEXPORT COMPLEX norm(const MV_Vector_COMPLEX &x);

#endif

// _MV_BLAS1_COMPLEX_H_
