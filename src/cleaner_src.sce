mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
exec(get_absolute_file_path('cleaner_src.sce')+'mv/cleaner_src.sce',-1)
exec(get_absolute_file_path('cleaner_src.sce')+'nii/cleaner_src.sce',-1)
exec(get_absolute_file_path('cleaner_src.sce')+'sparselib/cleaner_src.sce',-1)
exec(get_absolute_file_path('cleaner_src.sce')+'spblas/cleaner.sce',-1)
