// ====================================================================
// Copyright (C) 2011 - Benoit Goepfert
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

src_dir = get_absolute_file_path("builder_src.sce");

// Caution! 
// The order matters !
tbx_builder_src_lang("spblas", src_dir);
tbx_builder_src_lang("mv", src_dir);
tbx_builder_src_lang("sparselib", src_dir);
tbx_builder_src_lang("nii", src_dir);

clear tbx_builder_src_lang;
clear src_dir;
