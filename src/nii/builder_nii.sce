// ====================================================================
// Copyright (C) 2011 - Benoit Goepfert
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

src_dir = get_absolute_file_path("builder_nii.sce");

tbx_builder_src_lang("src", src_dir);
tbx_builder_src_lang("fortran", src_dir);

clear tbx_builder_src_lang;
clear src_dir;
