C===========================================================================//
C Copyright (C) 2011 - DIGITEO - Michael Baudin                             //
C Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  //
C Copyright (C) 2011 - National Institute of Informatics - Keiichi Morikuni  //
C                                                                           //
C This file must be used under the terms of the CeCILL.                     //
C This source file is licensed as described in the file COPYING, which      //
C you should have received as part of this distribution.  The terms         //
C are also available at                                                     //
C http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //
C===========================================================================//


C  Bibliography
C  Morikuni, K. and Hayami, K., 
C  Inner-iteration Krylov subspace methods for least squares problems, 
C  NII Technical Reports, National Institute of Informatics, 
C  Tokyo, NII-2011-001E, pp. 1-27, April, 2011. 
C  http://www.nii.ac.jp/TechReports/11-001E.html

C bagmres --
C A BA-GMRES NR-SOR solver for linear least squares problems.
C
C Arguments
C   Input
C     m: the number of rows (i.e. the number of equations)
C     n: the number of columns (i.e. the nubmer of unknowns)
C     nnz: the number of nonzeros
C     valAC(1:nnz): the nonzero entries, column-by-column (CCS format)
C     rowposA(1:nnz): the row indices of the nonzeros
C     colptrA(1:n+1): the start index of each column in valAC
C     valAR(1:nnz): the nonzero entries, row-by-row (CRS format)
C     colposA(1:nnz): the column indices of the nonzeros
C     rowptrA(1:m+1): the start index of each row in valAR
C     b(1:m): the right hand side
C     nbin: the number of inner iterations
C     maxout: the maximum number of outter iterations
C     eps: the tolerance to stop the algorithm
C     omg: the acceleration parameter for the SOR method
C     x(1:n): the solution of the linear least squares problem
C     res(1:maxout): norm of the residual r=A^(T)b-A^(T)Ax
C     nbout:  total number of outter iterations necessary to find the solution vector
C     conv: the status of the computation, see below
C     t_in: inner iterations computation time of for the iterative method
C     t_out: outter iterations computation time for the iterative method
C     
C Description
C    This function solves the sparse linear least squares problem min |Ax-b|, 
C    with the BA-GMRES method with NR-SOR preconditionning.
C 
C    The status of the computation is
C    conv = 0 if tolerance is reached
C    conv = 1 if no convergence
C    conv = 2 if ||aj|| is zero at j
C    conv = 3 if breakdown at k-th step
C
C    The GMRES method consist in solving the least square problem min(b-Ax), 
C    for A square matrix. 
C    But when A is not square, GMRES can't be directly used and need to be slightly modified.
C
C    The BA-GMRES method consist in solving the least square problem min(Bb-BAx), 
C    ie finding x such as the norm of Bb-BAx is minimal. 
C    This method is mainly useful for overdetermined problems, ie m > n for 
C    A of size m x n, because the matrix obtained by doing the product of 
C    B and A is of size n x n. 
C    However, as AB-GMRES isn't implemented for this preconditioner, 
C    underdetermined systems can be given too.
C 
C    The Normal Residual Successive Over-Relaxation method (NR-SOR) 
C    is a preconditioning technique applied with an iterative method 
C    (such as GMRES or CGLS) for solving least square problems. 
C    The SOR method is an iterative technique using an acceleration 
C    parameter used to solve a linear system of equations. 
C    NR-SOR is a particular case of this method, where an updated 
C    residual is used for the computations. 
C    However, on the contrary of other preconditionning methods such 
C    as Greville or RIF, the preconditioner B isn't computed before 
C    the algorithm. 
C    Thus, in the BA-GMRES algorithm, the NR-SOR method is regulary 
C    called to roughly solve a linear system instead of using the 
C    precomputed preconditioner B. 
C    The speed and accuracy of this method strongly depend on the number of 
C    inner iterations performed at each round. 
C 
C    The stopping criterion used is (A^T)(r_k) < tol * (A^T)(r_0).
C     

      subroutine bagmres(m, n, nnz, 
     *     valAC, rowposA, colptrA, 
     *     valAR, colposA, rowptrA, 
     *     b, nbin, maxout, eps, omg, 
     *     x, res, nbout, conv, t_in, t_out, 
     *     H, V, Aei, c, g, 
     *     r, s, w, y)

        implicit none 
        double precision :: zero = 0.0D0, one = 1.0D0
        common zero, one

        integer, intent(in) :: nbin, maxout, m, n, nnz
        double precision, intent(in) :: valAC(nnz)
        integer, intent(in) :: rowposA(nnz), colptrA(n+1)
        double precision, intent(in) :: valAR(nnz)
        integer, intent(in) :: colposA(nnz), rowptrA(m+1)
        double precision, intent(in) :: b(m), eps, omg
        integer, intent(inout) :: conv
        double precision, intent(inout) :: t_in, t_out
        integer, intent(out) :: nbout 
        double precision, intent(out) :: res(maxout), x(n)
C Work arrays
        double precision, intent(out) :: H(maxout+1, maxout), 
     *     V(n, maxout+1), Aei(n)
        double precision, intent(out) :: c(maxout), g(maxout+1), 
     *     r(m), s(maxout), w(n), y(maxout)
        double precision :: beta, inprod, tmp, tol, d, t_out1, t_out2,
     *              t_in1, t_in2
        integer :: i, j, k, l, k_, k1, k2, kk=1
        integer INCB, INCR

        kk=kk+1

        call cpu_time(t_out1)

c calculate 1/(||a_i||_2)^2
        do j = 1, n
            inprod = zero
            do l = colptrA(j), colptrA(j+1)-1
                inprod = inprod + valAC(l)*valAC(l)
            enddo
            Aei(j) = one / inprod 
            if (Aei(j)*zero /= zero) then
c               write(*, *) 'warning: ||aj|| = zero at j =',j
                conv = 2
                nbout = j
                return
            endif
        enddo
c       calculate w = A^T * r_0	
        do j = 1, n
            inprod = zero
            do l = colptrA(j), colptrA(j+1)-1
                inprod = inprod + valAC(l)*b(rowposA(l))
            enddo
            w(j) = inprod
c           write(*, *) 'w(',j,') = ',w(j)
        enddo
c       calculate tol = eps * ||w||		
        inprod = zero
        do j = 1, n
            inprod = inprod + w(j)*w(j)
        enddo
        tol = eps * sqrt(inprod)

c       put r_0 = b (case when x_0 = (0, ..., 0)^T)
        INCB = 1
        INCR = 1
        call dcopy(m, b, INCB, r, INCR)

        call cpu_time(t_out2)
        t_out=t_out+t_out2-t_out1

        call cpu_time(t_in1)

c roughly solve with NR-SOR(inner iteration) z = B * r_0
        do j = 1, n
            V(j,1) = zero
        enddo

        do k = 1, nbin 
            do j = 1, n
            d = zero
                k1 = colptrA(j)
                k2 = colptrA(j+1)-1
                do l = k1, k2
                    d = d + r(rowposA(l))*valAC(l)
                enddo
                d = omg * d * Aei(j)
                V(j,1) = V(j,1) + d
                if (k == nbin .and. j == n) then
                    goto 100
                endif
                do l = k1, k2
                    r(rowposA(l)) = r(rowposA(l)) - d*valAC(l)
                enddo
            enddo
        enddo
 100    continue

        call cpu_time(t_in2)
        t_in=t_in+t_in2-t_in1 

        call cpu_time(t_out1)

        inprod = zero
        do j = 1, n
            inprod = inprod + V(j, 1)*V(j, 1)
        enddo
        beta = sqrt(inprod)

        tmp = one / beta
        do j = 1, n
            V(j, 1) = tmp * V(j, 1)
        enddo

        g(1) = beta

        kk=kk+1
        
        if (m > n) then
            do k = 1, maxout
c               for overdetermined systems
                do j = 1, m
                    r(j) = zero
                enddo
                do j = 1, n
                    tmp = V(j, k)
                    do l = colptrA(j), colptrA(j+1)-1
                        i = rowposA(l)
                        r(i) = r(i) + tmp*valAC(l)
                    enddo
                enddo

                call cpu_time(t_out2)
                t_out=t_out+t_out2-t_out1

                call cpu_time(t_in1)

c               Inner Iteration (NRSOR)
                do j = 1, n
                    w(j) = zero
                enddo

                do k_ = 1, nbin 
                    do j = 1, n
                        d = zero
                        k1 = colptrA(j)
                        k2 = colptrA(j+1)-1
                        do l = k1, k2
                            d = d + r(rowposA(l))*valAC(l)
                        enddo
                        d = omg * d * Aei(j)
                        w(j) = w(j) + d
                        if (k_ == nbin .and. j == n) then
                            goto 200
                        endif
                        do l = k1, k2
                            r(rowposA(l)) = r(rowposA(l)) - d*valAC(l)
                        enddo
                    enddo
                enddo
 200            continue

                call cpu_time(t_in2)
                t_in=t_in+t_in2-t_in1
                
                call cpu_time(t_out1)

c                Gram-Schmidt process
                do i = 1, k
                    inprod = zero
                    do j = 1, n
                        inprod = inprod + w(j)*V(j, i)
                    enddo
                    H(i, k)	= inprod 

                    tmp = H(i, k)
                    do j = 1, n
                        w(j) = w(j) - tmp*V(j, i)
                    enddo
                enddo

                inprod = zero
                do j = 1, n
                    inprod = inprod + w(j)*w(j)
                enddo
                H(k+1, k) = sqrt(inprod)

                if (H(k+1, k) == zero) then
c                   write(*, *) 'BREAKDOW at', k, '-th step'
                    conv = 3
                    nbout = k - 2
                    call cpu_time(t_out2)
                    t_out=t_out+t_out2-t_out1
                    return
                endif

                tmp = one / H(k+1, k)
                do j = 1, n
                    V(j, k+1) = tmp * w(j)
                enddo

c                Given's rotation
                do i = 1, k-1
                    tmp =  c(i)*H(i, k) + s(i)*H(i+1, k)
                    H(i+1, k) = -s(i)*H(i, k) + c(i)*H(i+1, k)
                    H(i, k) = tmp
                enddo

                call drotg(H(k, k), H(k+1, k), c(k), s(k))

                g(k+1) = -s(k)*g(k)
                g(k) = c(k)*g(k)

                H(k, k) = one / H(k, k)

c                if (abs(g(k+1)) / beta < eps) then	
c                    Backward substitution
                    y(k) = g(k) * H(k, k)
                    do i = k-1, 1, -1
                        inprod = zero 
                        do l = i+1, k
                        	inprod = inprod + H(i, l)*y(l)
                        enddo
                        y(i) = (g(i) - inprod) * H(i, i)
                    enddo

                    x(1:n) = matmul(V(1:n, 1:k), y(1:k))

c                    for overdetermined systems
                    do j = 1, m
                        r(j) = zero
                    enddo
                    do j = 1, n
                        tmp = x(j)
                        do l = colptrA(j), colptrA(j+1)-1
                        	i = rowposA(l)
                        	r(i) = r(i) + tmp*valAC(l)
                        enddo
                    enddo
                    do i = 1, m
                        r(i) = b(i) - r(i)
                    enddo

                    do j = 1, n
                        inprod = zero
                        do l = colptrA(j), colptrA(j+1)-1
                        	inprod = inprod + valAC(l)*r(rowposA(l))
                        enddo
                        w(j) = inprod
                    enddo

                    inprod = zero
                    do j = 1, n
                        inprod = inprod + w(j)*w(j)
                    enddo
                    res(k) = sqrt(inprod) 

                    if ((res(k)) < tol) then
                        conv = 0
                        nbout = k
                        call cpu_time(t_out2)
                        t_out=t_out+t_out1-t_out2
                        return
                    endif
c                endif

                if (k == maxout) then
c                    No convergence
                    conv = 1
                    nbout = k

                    y(nbout) = g(nbout) * H(nbout, nbout)
                    do i = nbout-1, 1, -1
                        inprod = zero 
                        do l = i+1, nbout
                        	inprod = inprod + H(i, l)*y(l)
                        enddo
                        y(i) = (g(i) - inprod) * H(i, i)
                    enddo

                    x(1:n) = matmul(V(1:n, 1:nbout), y(1:nbout))
                    call cpu_time(t_out2)
                    t_out=t_out+t_out1-t_out2
                    return	
                endif
            enddo
        else
            do k = 1, maxout 
c                for underdetermined systems 
                do i = 1, m
                    tmp = zero
                    do l = rowptrA(i), rowptrA(i+1)-1
                        tmp = tmp + valAR(l)*V(colposA(l), k)
                    enddo
                    r(i) = tmp
                enddo

                call cpu_time(t_out2)
                t_out=t_out+t_out2-t_out1

                call cpu_time(t_in1)

c               Inner Iteration (NRSOR)
                do j = 1, n
                    w(j) = zero
                enddo

                do k_ = 1, nbin 
                    do j = 1, n
                        d = zero
                        k1 = colptrA(j)
                        k2 = colptrA(j+1)-1
                        do l = k1, k2
                        	d = d + r(rowposA(l))*valAC(l)
                        enddo
                        d = omg * d * Aei(j)
                        w(j) = w(j) + d
                        if (k_ == nbin .and. j == n) then
                        	goto 300
                        endif
                        do l = k1, k2
                        	r(rowposA(l)) = r(rowposA(l)) - d*valAC(l)
                        enddo
                    enddo
                enddo                
 300            continue
                
                call cpu_time(t_in2)
                t_in=t_in+t_in2-t_in1
                
                call cpu_time(t_out1)	
                
c                Gram-Schmidt process
                do i = 1, k
                    inprod = zero
                    do j = 1, n
                        inprod = inprod + w(j)*V(j, i)
                    enddo
                    H(i, k)	= inprod 
                    tmp = H(i, k)
                    do j = 1, n
                        w(j) = w(j) - tmp*V(j, i)
                    enddo
                enddo

                inprod = zero
                do j = 1, n
                    inprod = inprod + w(j)*w(j)
                enddo
                H(k+1, k) = sqrt(inprod)
                if (H(k+1, k) == zero) then
c                    write(*, *) 'BREAKDOW at', k, '-th step'
                    conv = 3
                    nbout = k - 2
                    call cpu_time(t_out2)
                    t_out=t_out+t_out2-t_out1
                    return
                endif

                tmp = one / H(k+1, k)
                do j = 1, n
                    V(j, k+1) = tmp * w(j)
                enddo

c                Given's rotation
                do i = 1, k-1
                    tmp =  c(i)*H(i, k) + s(i)*H(i+1, k)
                    H(i+1, k) = -s(i)*H(i, k) + c(i)*H(i+1, k)
                    H(i, k) = tmp
                enddo

                call drotg(H(k, k), H(k+1, k), c(k), s(k))

                g(k+1) = -s(k)*g(k)
                g(k) = c(k)*g(k)

                H(k, k) = one / H(k, k)

c               if (abs(g(k+1)) / beta < eps) then	
c                   Backward substitution
                    y(k) = g(k) * H(k, k)
                    do i = k-1, 1, -1
                        inprod = zero 
                        do l = i+1, k
                            inprod = inprod + H(i, l)*y(l)
                        enddo
                        y(i) = (g(i) - inprod) * H(i, i)
                    enddo
                    x(1:n) = matmul(V(1:n, 1:k), y(1:k))
                   
c                    for underdetermined systems
                    do i = 1, m
                        inprod = zero
                        do l = rowptrA(i), rowptrA(i+1)-1
                        	inprod = inprod + valAR(l)*x(colposA(l))
                        enddo
                        r(i) = inprod
                    enddo
                    do i = 1, m
                        r(i) = b(i) - r(i)
                    enddo                
                    do j = 1, n
                        inprod = zero
                        do l = colptrA(j), colptrA(j+1)-1
                        	inprod = inprod + valAC(l)*r(rowposA(l))
                        enddo
                        w(j) = inprod
                    enddo
                    inprod = zero
                    do j = 1, n
                        inprod = inprod + w(j)*w(j)
                    enddo
                    res(k) = sqrt(inprod) 
c                    write(*, *) res(k)
c                    write(*, *) tol
                    if ((res(k)) < tol) then
                        conv = 0
                        nbout = k
                        call cpu_time(t_out2)
                        t_out=t_out+t_out1-t_out2
                        return
                    endif
c                endif

                if (k == maxout) then
c                   No convergence
                    conv = 1
                    nbout = k

                    y(nbout) = g(nbout) * H(nbout, nbout)
                    do i = nbout-1, 1, -1
                        inprod = zero 
                        do l = i+1, nbout
                            inprod = inprod + H(i, l)*y(l)
                        enddo
                        y(i) = (g(i) - inprod) * H(i, i)
                    enddo

                    x(1:n) = matmul(V(1:n, 1:nbout), y(1:nbout))
                    call cpu_time(t_out2)
                    t_out=t_out+t_out1-t_out2
                    return	
                endif
            enddo
         endif
      end subroutine bagmres

