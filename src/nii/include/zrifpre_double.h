/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* Copyright (C) 2007 - National Institute of Informatics - Jun-Feng Yin     */
/* Copyright (C) 2011 - DIGITEO - Michael Baudin                             */
/* Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  */
/*                                                                           */
/* This file must be used under the terms of the CeCILL.                     */
/* This source file is licensed as described in the file COPYING, which      */
/* you should have received as part of this distribution.  The terms         */
/* are also available at                                                     */
/* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*                                                                           */
/*                     A Package based on                                    */
/*                     SparseLib++ : Sparse Matrix Library                   */
/*                                                                           */
/*                                 NOTICE                                    */
/*                                                                           */
/* Permission to use, copy, modify, and distribute this software and         */
/* its documentation for any purpose and without fee is hereby granted       */
/* provided that the above notice appear in all copies and supporting        */
/* documentation.                                                            */
/*                                                                           */
/* Neither the Institutions (Institute of Computational Mathematics and      */
/* Scientific/Engineering Computing, Chinese Academy of Sciences) nor the    */
/* Authors make any representations about the suitability of this software   */
/* for any purpose.  This software is provided ``as is'' without expressed   */
/* or implied warranty.                                                      */
/*                                                                           */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#ifndef ZRIFPRE_H
#define ZRIFPRE_H

#ifdef _MSC_VER
	#if LIBSPLSPC_EXPORTS 
		#define SPLSPC_IMPORTEXPORT __declspec (dllexport)
	#else
		#define SPLSPC_IMPORTEXPORT __declspec (dllimport)
	#endif
#else
	#define SPLSPC_IMPORTEXPORT
#endif

#include "vecdefs.h"
#include VECTOR_H
#include MATRIX_H
#include "comprow_double.h"
#include "compcol_double.h"


class SPLSPC_IMPORTEXPORT CompCol_ZRIFPreconditioner_double {

 private:
	VECTOR_double d_val_;
	VECTOR_double sd_val_;
	VECTOR_double z_val_;
	VECTOR_int    z_colptr_;
	VECTOR_int    z_rowind_;
	int z_nz_;
	int dim_[2];
  
 public:
	CompCol_ZRIFPreconditioner_double(void);
	CompCol_ZRIFPreconditioner_double(const CompCol_Mat_double &A, const double tau, double& pretime, int &cancel);
	~CompCol_ZRIFPreconditioner_double(void){};
	
	VECTOR_double 	  Bsolve(const CompCol_Mat_double &A, const VECTOR_double &x) const;
	VECTOR_double 	  solve(const CompCol_Mat_double &A, const VECTOR_double &x) const;
	VECTOR_double 	  trans_solve(const CompCol_Mat_double &A, const VECTOR_double &x) const;

	//getters
	double *get_D_val(void);
	double *get_SD_val(void);
	double *get_Z_val(void);
	int *get_Z_colptr(void);
	int *get_Z_rowind(void);
	int get_Z_nz(void);
	int *get_dims(void);
};



#endif
