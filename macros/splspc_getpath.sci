//===========================================================================//
// Copyright (C) 2011 - DIGITEO - Michael Baudin                             //
// Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  //
//                                                                           //
// This file must be used under the terms of the CeCILL.                     //
// This source file is licensed as described in the file COPYING, which      //
// you should have received as part of this distribution.  The terms         //
// are also available at                                                     //
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //
//===========================================================================//

function path = splspc_getpath (  )
    // Returns the path to the current module.
    // 
    // Calling Sequence
    //   path = splspc_getpath (  )
    //
    // Parameters
    //   path : a 1-by-1 matrix of strings, the path to the current module.
    //
    // Examples
    //   path = splspc_getpath (  )
    //   // See the provided documents in the doc directory.
    //   ls(fullfile(path,"doc"))
    //
    // Authors
    //   Copyright (C) 2011 - INRIA - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "splspc_getpath" , rhs , 0 )
    apifun_checklhs ( "splspc_getpath" , lhs , 1 )
    
    path = get_function_path("splspc_getpath")
    path = fullpath(fullfile(fileparts(path),".."))
endfunction

