//===========================================================================//
// Copyright (C) 2011 - DIGITEO - Michael Baudin                             //
// Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  //
//                                                                           //
// This file must be used under the terms of the CeCILL.                     //
// This source file is licensed as described in the file COPYING, which      //
// you should have received as part of this distribution.  The terms         //
// are also available at                                                     //
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //
//===========================================================================//

gateway_path = get_absolute_file_path("builder_gateway_cpp.sce");

libname = "splspcgatewaycpp";
namelist = [
"splspc_gmresba" "sci_gmresba"
"splspc_gmresab" "sci_gmresab"
"splspc_rifgmresba" "sci_rifgmresba"
"splspc_rifgmresab" "sci_rifgmresab"
"splspc_grevgmresba" "sci_grevgmresba"
"splspc_grevgmresab" "sci_grevgmresab"
"splspc_nrsorgmresba" "sci_nrsorgmresba"
];
files = [
"sci_gmresba.cpp"
"sci_gmresab.cpp"
"sci_rifgmresba.cpp"
"sci_rifgmresab.cpp"
"sci_grevgmresba.cpp"
"sci_grevgmresab.cpp"
"sci_nrsorgmresba.cpp"
"gw_splspc_support.cpp"
];

ldflags = ""

if ( MSDOS ) then
  include2 = gateway_path+"..\..\src\mv\include";
  include3 = gateway_path+"..\..\src\sparselib\include"; 
  include4 = gateway_path+"..\..\src\nii\include";    
  include5 = SCI+"\modules\output_stream\includes";
  include6 = gateway_path+"../../src/nii/fortran";             
  cflags = "-DWIN32 -DLIBSPLSPC_EXPORTS -I"""+...
		   include2+""" -I"""+include3+""" -I"""+...
		   include4+""" -I"""+include5+""" -I"""+include6+"""";
  libs = [
  "..\..\src\spblas\libspblas"
  "..\..\src\mv\src\libmv"    
  "..\..\src\sparselib\src\libsparselib"
  "..\..\src\nii\src\libniicpp"
  "..\..\src\nii\fortran\libniifortran"
  ]; 		   
			   
else
  include1 = gateway_path;
  include2 = gateway_path+"../../src/mv/include";
  include3 = gateway_path+"../../src/sparselib/include"; 
  include4 = gateway_path+"../../src/nii/include";     
  include5 = SCI+"/../../include/scilab/localization";
  include6 = SCI+"/../../include/scilab/output_stream";
  include7 = SCI+"/../../include/scilab/core";
  include8 = gateway_path+"../../src/nii/fortran";             
  cflags = "-ansi -g -Wall -pedantic -O3 -I"""+...
		   include1+""" -I"""+include2+""" -I"""+include3+""" -I"""+include4+...
		   """ -I"""+include5+""" -I"""+include6+""" -I"""+include7+...
		   """ -I"""+include8+"""";
  libs = [
  "../../src/spblas/libspblas"
  "../../src/mv/src/libmv"    
  "../../src/sparselib/src/libsparselib"
  "../../src/nii/src/libniicpp"
  "../../src/nii/fortran/libniifortran"
  ];  
end

tbx_build_gateway(libname, namelist, files, gateway_path, libs, ldflags, cflags);

clear tbx_build_gateway;


