
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//                                                                           //
// This file must be used under the terms of the CeCILL.                     //
// This source file is licensed as described in the file COPYING, which      //
// you should have received as part of this distribution.  The terms         //
// are also available at                                                     //
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  //

//
// gw_splpsc.h
//   Header for the SPLSPC gateway.
//
#ifndef __SCI_GW_SPLSPC_H__
#define __SCI_GW_SPLSPC_H__

extern "C" {

	// Functions providing interfaces to Scilab functions
	//
	int sci_gmresba (char *fname);
	int sci_gmresab (char *fname);
	int sci_rifgmresba (char *fname);
	int sci_rifgmresab (char *fname);
	int sci_grevgmresba (char *fname);
	int sci_grevgmresab (char *fname);
	int sci_nrsorgmresba (char *fname);

}
#endif /* __SCI_GW_SPLSPC_H__ */
