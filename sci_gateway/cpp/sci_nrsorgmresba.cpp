/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* Copyright (C) 2011 - DIGITEO - Michael Baudin                             */
/* Copyright (C) 2011 - National Institute of Informatics - Benoit Goepfert  */
/*                                                                           */
/* This file must be used under the terms of the CeCILL.                     */
/* This source file is licensed as described in the file COPYING, which      */
/* you should have received as part of this distribution.  The terms         */
/* are also available at                                                     */
/* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt                  */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*C includes*/
extern "C" {
#include "stdio.h"
#include "stdlib.h"
}
#include <time.h>

/*API scilab includes*/
extern "C" {
#include "stack-c.h"
#include "api_scilab.h" 
#include "Scierror.h"
#include "localization.h"
#include "sciprint.h"
#include "gw_splspc.h"
#include "gw_splspc_support.h"
}

/*local includes*/
extern "C" {
#include "nrsor.h"
}

int sci_nrsorgmresba (char *fname)
{ 
	int iRowsA, iColsA, iRowsb, iColsb, i, j, cpt;
	int nbin, nbout, iNnzA, maxiter, sciErrNb, result=0;
	double tol, omg, intime=0.0, outtime=0.0;
	int *piNbItemRowA=NULL, *piColPosA=NULL, *piRowPtrA=NULL, *piAddr=NULL;
	int *piNbItemColA=NULL, *piRowPosA=NULL, *piColPtrA=NULL, *piColPtrAtmp=NULL;
	double *b=NULL, *x=NULL, *pdblRealAR=NULL, *pdblRealAC=NULL, *res=NULL;

	double *Hwork=NULL, *Vwork=NULL, *Aeiwork=NULL;
	double *cwork=NULL, *gwork=NULL, *rwork=NULL, *swork=NULL;
	double *wwork=NULL, *ywork=NULL;

	int readFlag;

	SciErr sciErr;

	CheckRhs(2,6);
	CheckLhs(1,3);

	/*get input argument 1: matrix A*/
	readFlag = splspc_getmatrix (fname, 1, &iRowsA,  &iColsA,  &iNnzA,  &piNbItemRowA, 
		&piColPosA,  &pdblRealAR,  &piRowPtrA);
	if (readFlag==0)
	{
		return 0;
	}

	/*get input argument 2: vector b*/
	readFlag = splspc_getvector (fname, 2, iRowsA, &iRowsb, &iColsb, &b);
	if (readFlag==0)
	{
		return 0;
	}

	/* get input argument 3: double omg */
	readFlag = splspc_getScalarDouble (fname, 3, Rhs, 1.0, &omg);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_checkDoubleInRange (fname, 3, omg, 0., DBL_MAX);
	if (readFlag==0)
	{
		return 0;
	}

	/* get input argument 4: int nbin */
	readFlag = splspc_getScalarIntegerFromScalarDouble (fname, 4, Rhs, 1, &nbin);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_checkIntegerInRange (fname, 4, nbin, 0, INT_MAX);
	if (readFlag==0)
	{
		return 0;
	}

	/* get input argument 5: int maxiter */
	readFlag = splspc_getArgumentMaxiter (fname, 5, iRowsA, iColsA, &maxiter);
	if (readFlag==0)
	{
		return 0;
	}

	/* get input argument 6: double tol */
	readFlag = splspc_getArgumentTol (fname, 6, &tol);
	if (readFlag==0)
	{
		return 0;
	}

	// convert matrix A from CRS to CCS and get the 2 new vectors (colptr and rowind)
	//
	// Compressed Row Sparse:
	// iRowsA: number of rows in A
	// iColsA: number of columns in A
	// iNnzA: the number of nonzeros
	// piNbItemRowA[0,1,...,iRowsA-1]: the number of nonzeros in each row
	// piColPosA[0,1,...,iNnzA-1]: the column indices of the nonzeros
	// pdblRealAR[0,1,...,iNnzA-1]: the nonzero values, row-by-row
	// piRowPtrA[0,1,...,iRowsA-1]: the starting index of each row in pdblRealAR
	//
	// Compressed Column Sparse:
	// piNbItemColA[0,1,...,iColsA-1]: the number of nonzeros in each column
	// piRowPosA[0,1,...,iNnzA-1]: the row indices of the nonzeros
	// pdblRealAC[0,1,...,iNnzA-1]: the nonzero values, column-by-column
	// piColPtrA[0,1,...,iColsA-1]: the starting index of each column in pdblRealAC
	//
	readFlag = splspc_allocIntVector (fname, iColsA+1, &piNbItemColA);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocIntVector (fname, iColsA+1, &piColPtrA);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocIntVector (fname, iNnzA, &piRowPosA);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, iNnzA, &pdblRealAC);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocIntVector (fname, iColsA+1, &piColPtrAtmp);
	if (readFlag==0)
	{
		return 0;
	}

	// compute the number of non zeros entries per column, and then the columns pointers array
	for (i=0;i<iColsA+1;i++)
	{
		piNbItemColA[i]=0;
	}
	for (i=0;i<iNnzA;i++)
	{
		piNbItemColA[piColPosA[i]]++;
	}
	piColPtrA[0] = 0;
	for (j=1;j<iColsA+1;j++)
	{
		piColPtrA[j] = piColPtrA[j-1]+piNbItemColA[j-1];
	}
	memcpy(piColPtrAtmp,piColPtrA,(iColsA+1)*sizeof(int));
	// compute the arrays of row positions and the array of values
	cpt = 0;
	for (i=1;i<=iRowsA;i++)
	{
		for (j=cpt;j<piRowPtrA[i];j++)
		{
			pdblRealAC[piColPtrAtmp[piColPosA[j]]] = pdblRealAR[j];
			piRowPosA[piColPtrAtmp[piColPosA[j]]] = i-1;
			piColPtrAtmp[piColPosA[cpt]]++;
			cpt++;
		}
	}
	for (j=0;j<iColsA+1;j++)
	{
		piColPtrA[j]++;
	}
	for (j=0;j<iNnzA;j++)
	{
		piRowPosA[j]++;
	}
	for (j=0;j<iRowsA+1;j++)
	{
		piRowPtrA[j]++;		
	}
	for (j=0;j<iNnzA;j++)
	{
		piColPosA[j]++;
	}		

	/*allocate x and res vectors */
	readFlag = splspc_allocDoubleVector (fname, iColsA, &x);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, maxiter, &res);
	if (readFlag==0)
	{
		return 0;
	}

	// Allocate memory for the Fortran routine
	readFlag = splspc_allocDoubleVector (fname, (maxiter+1)*maxiter, &Hwork);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, (maxiter+1)*iColsA, &Vwork);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, iColsA, &Aeiwork);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, maxiter, &cwork);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, maxiter+1, &gwork);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, iRowsA, &rwork);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, maxiter, &swork);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, iColsA, &wwork);
	if (readFlag==0)
	{
		return 0;
	}
	readFlag = splspc_allocDoubleVector (fname, maxiter, &ywork);
	if (readFlag==0)
	{
		return 0;
	}

	F2C(bagmres)(&iRowsA, &iColsA, &iNnzA, 
		pdblRealAC, piRowPosA, piColPtrA, 
		pdblRealAR, piColPosA, piRowPtrA, 
		b, &nbin, &maxiter, &tol, &omg, 
		x, res, &nbout, &result, &intime, &outtime, 
		Hwork, Vwork, Aeiwork, cwork, gwork, 
		rwork, swork, wwork, ywork);

	// Free memory
	free(Hwork);
	free(Vwork);
	free(Aeiwork);
	free(cwork);
	free(gwork);
	free(rwork);
	free(swork);
	free(wwork);
	free(ywork);

	if (result==2)
	{
		Scierror(204,_("%s: ||aj|| = zero at j = %d\n"),fname, nbout);
		return 0;		
	}
	if (result==3)
	{
		Scierror(204,_("%s: breakdown at %d-th step\n"),fname, nbout+2);
		return 0;		
	}
	if (result!=0)
	{
		splspc_warningNonconvergence(fname);
	}

	/*create ouput argument 1 : vector x*/
	sciErr=createMatrixOfDouble(pvApiCtx, Rhs + 1, iColsA, 1, x);

	/*create ouput argument 2 : int nbout*/
	sciErrNb=createScalarDouble(pvApiCtx, Rhs + 2, nbout);

	/*create output argument 3 : vector res*/
	sciErr=createMatrixOfDouble(pvApiCtx, Rhs + 3, maxiter, 1, res);

	// Free the memory of 
	free(x);
	free(res);
	free(piNbItemColA);
	free(piColPtrA);
	free(piRowPosA);
	free(pdblRealAC);
	free(piColPtrAtmp);


	LhsVar(1) = Rhs + 1;
	if (Lhs>=2){
		LhsVar(2) = Rhs + 2;
	}
	if (Lhs>=3){
		LhsVar(3) = Rhs + 3;
	}
	return 0;
}
